package Prueba

import org.apache.spark.{SparkConf, SparkContext}

object Practica2 extends App {

  System.setProperty("hadoop.home.dir", "C:/Hadoop")
  val conf = new SparkConf().setAppName("Practica2").setMaster("local")
  val sc = new SparkContext(conf)

  val alogs = sc.textFile("C:\\Users\\Bosonit\\Desktop\\Datasetsparaalumnos\\data_spark\\weblogs\\*")


  var rdd = alogs.map(_.split(" ")).map(words=> (words(2), 1))
  var rdd2 = rdd.reduceByKey((v1,v2) => v1 + v2)
  rdd2.map(_.swap).sortByKey(false).map(_.swap).take(5).foreach(println)

  var userreq = alogs.map(_.split(" ")).map(w => ( w(2) , w(0))).groupByKey()
  userreq.take(10).foreach(println)


  for (x <- userreq.take(15)){
    println("ID:" + x._1 )
    println("Ips:")
    x._2.foreach(println)
  }

}
